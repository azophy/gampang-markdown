<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Simple Markdown Editor</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

#result table {
  border-collapse: collapse;
  border: 1px solid black;
}

#result table td, #result table th {
  border: 1px solid black;
  padding: 5px;
}
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">Markdown Gampang</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Panduan-panduan</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a target="_blank" class="dropdown-item" href="https://github.com/showdownjs/showdown/wiki/Showdown's-Markdown-syntax">Panduan Markdown Umum</a>
          <a target="_blank" class="dropdown-item" href="https://obedm503.github.io/showdown-katex/#moreexamples">Panduan penulisan rumus matematika</a>
          <a target="_blank" class="dropdown-item" href="https://katex.org/docs/supported.html">List fungsi2 latex di katex</a>
        </div>
      </li>
    </ul>
  </div>
</nav>

<main role="main" class="container-fluid py-5">
    <h1>Super minimalist markdown editor</h1>
    <div class="row">
        <div class="col">
            <h3>Masukan markdown mentah</h3>
    <textarea id="input" name="" style="width:100%;height:80vh">
test bikin markdown
=========

## test rumus yang bisa

* format latex $$ la = te^x $$
* format asciimath ~ asc^ii = ma(th) ~

## yang ini gak bisa 

$ a = b^2 $

    </textarea>
        </div>
        <div class="col">
            <h3>Hasil:</h3>
    <div id="result" style="background:#ddd;width:100%;height:80vh"></div>
        </div>
    </div>
</main><!-- /.container -->
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/showdown@1.9.1/dist/showdown.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/showdown-katex@0.6.0/dist/showdown-katex.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css">
<script>

// from https://stackoverflow.com/a/20618592
var converter = null, 
    is_tryout=false;

function do_convert() {

    converter = new showdown.Converter({
            parseImgDimensions: true,
            tables: true,
            smoothLivePreview: true,
            extensions: [
                showdownKatex({}),
            ],
        }),
        markdown_list = document.getElementsByClassName('markdown');

    document.getElementById('result').innerHTML = converter.makeHtml(document.getElementById('input').value);
};
document.getElementById('input').onkeyup = do_convert;
do_convert();
</script>
</html>
